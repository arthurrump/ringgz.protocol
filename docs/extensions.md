# Extensions to the protocol

For some of the extensions mentioned in the module guide, extensions to the protocol are needed as well. To ensure that these extensions don't break communication with servers or clients that don't support the same extensions, the extensions that are supported on both sides is agreed upon at login. This document describes the flow of messages, for a description of each message and its parameters, check out the [Javadoc](https://arthurrump.gitlab.io/ringgz.protocol/ringgz/protocol/Commands.html#method.detail), messages used in extensions are tagged with `[EXT (extension name)]`.

## Chat Box
Indication at login: `chat`

To send a chat message, the client sends the command `send-chat <message>`. The message can contain any character, except for a newline, as that indicates the end of a command. Anything after `send-chat ` can be regarded as chat message. When the server receives the send-chat command, it will send the message to all clients participating in the same game that indicated at login that they support the chat functionality. This is done with the `chat <username> <message>` command.

### Example
```
C: login Pug chat
S: login ok chat
.
.
S: chat Nakor Hey there! I am using Ringgz chat.
S: chat MacrosTheBlack Hey Nakor, I don't think you can cheat with this game ;P
C: send-chat @MacrosTheBlack of course he can, it's Nakor!
S: chat Pug @MacrosTheBlack of course he can, it's Nakor!
.
```

## Challenge
Indication at login: `challenge`

When using the challenge extension, players can challenge other players that also support challenges. All commands in the challenge extension replace the make-game command in the basic flow.

After logging in, this is the flow for a challenge:

1. Client: `request-players`
2. Server: `players <username> <username> ...`
3. Client: `challenge <username> [<username> <username>]`

    The command contains a list of usernames that will participate in the game, except for the username of the player making the request.

4. Server: `invite <username> <username> [<username> <username>]`

    This command contains a list of all usernames that will participate in the game, including the challenger. This message is send to all challenged players, not to the challenger.

5. Other clients: `invite accepted` if the player accepts the challenge, `invite denied` if the player denies

    All clients that received the invite message, respond with either accepted or denied.

6. Server: `challenge denied` if one of the clients denied the challenge, otherwise `game-started ...` as in the basic protocol.

    These messages are sent to all players in the challenge, including the challenger.

If the challenge was accepted and the game-started message was sent, game flow will continue as in the basic protocol. If the challenge was denied, the player can choose to challenge other players or participate in a random game with the make-game command.

### Examples
In the next examples three clients are shown, `C1`, `C2` and `C3`. The numbers after the `S` indicate to which client the message is sent, for example `S13` means a message from the server to clients 1 and 3.

```
C1  : login Nakor challenge
S1  : login ok challenge
C2  : login Pug challenge
S2  : login ok challenge
C3  : login Miranda challenge
S3  : login ok challenge
C2  : request-players
S2  : players Nakor Miranda
C2  : challenge Miranda Nakor
S13 : invite Pug Miranda Nakor
C1  : invite accepted
C3  : invite accepted
S123: game-started Pug(P,R) Nakor(Y,R) Miranda(G,R)
.
.
```

```
<login as in previous example>
C2  : request-players
S2  : players Nakor Miranda
C2  : challenge Miranda Nakor
S13 : invite Pug Miranda Nakor
C1  : invite accepted
C3  : invite denied
S123: challenge denied
.
.
```

## Leaderboard
Indication at login: `leaderboard`

The client can request the leaderboard at any time by sending the `get-leaderboard` command. The server will respond with `leaderboard <username>(<score>) <username>(<score>) ...`. The server is free to choose a score to use with the leaderboard, as long as it is an integer. The leaderboard sent by the server isn't required to be sorted, the client is responsible for displaying it in a logical way.

### Example
```
C: login Tomas leaderboard
S: login ok leaderboard
.
.
C: get-leaderboard
S: leaderboard Pug(31) Nakor(53) Tomas(22) MacrosTheBlack(32) Aglaranna(23)
.
```
