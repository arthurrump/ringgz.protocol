import org.junit.jupiter.api.Test;
import ringgz.protocol.Commands;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for all commands for the chat extension.
 * @author Arthur Rump
 */
public class ChatCommandsTest {
    @Test
    public void sendChatTest() {
        assertEquals("send-chat Lorem ipsum Jupiter deus est. In Olympo habitat.",
                Commands.sendChat("Lorem ipsum Jupiter deus est. In Olympo habitat."));
    }

    @Test
    public void chatTest() {
        assertEquals("chat Disco Lorem ipsum Jupiter deus est. In Olympo habitat.",
                Commands.chat("Disco", "Lorem ipsum Jupiter deus est. In Olympo habitat."));
    }
}