import ringgz.protocol.Commands;
import ringgz.protocol.Commands.Extension;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.*;

/**
 * Tests for all basic commands in the Commands class.
 * @author Arthur Rump
 */
public class CommandsTest {
    @Test
    public void extensionsToStringTest() {
        Extension e1 = Extension.CHAT;
        Extension e2 = Extension.LEADERBOARD;

        assertEquals("chat", e1.toString());
        assertEquals("leaderboard", e2.toString());
    }

    @Test
    public void loginTest() {
        Extension[] empty = {};
        assertEquals("login Pug82", Commands.login("Pug82", empty));
        assertEquals("login Nakor challenge chat",
                Commands.login("Nakor", new Extension[]{Extension.CHALLENGE, Extension.CHAT}));

        assertThrows(IllegalArgumentException.class, () -> Commands.login("Invalid name", empty));
        assertThrows(IllegalArgumentException.class, () -> Commands.login("invalid.name", empty));
        assertThrows(IllegalArgumentException.class, () -> Commands.login("Invalid᠎Name", empty));
    }

    @Test
    public void loginFailTest() {
        assertEquals("login fail 1", Commands.loginFail(1));
    }

    @Test
    public void loginOkTest() {
        assertEquals("login ok", Commands.loginOk(new Extension[]{}));
        assertEquals("login ok leaderboard", Commands.loginOk(new Extension[]{Extension.LEADERBOARD}));
    }

    @Test
    public void makeGameTest() {
        assertEquals("make-game 3", Commands.makeGame(3));
    }

    @Test
    public void gameStartedTest() {
        Map<String, List<String>> usersColors = new HashMap<>();
        List<String> colors1 = new ArrayList<>();
        colors1.add("P");
        colors1.add("R");
        usersColors.put("Pug", colors1);
        List<String> colors2 = new ArrayList<>();
        colors2.add("G");
        colors2.add("Y");
        usersColors.put("Nakor", colors2);

        assertEquals("game-started Pug(P,R) Nakor(G,Y)", Commands.gameStarted(usersColors));
    }

    @Test
    public void makeMoveTest() {
        assertEquals("make-move 2 1 RGYP 4", Commands.makeMove(2, 1, "RGYP", 4));
        assertEquals("make-move 4 4 R 0", Commands.makeMove(4, 4, "R", 0));
    }

    @Test
    public void invalidMoveTest() {
        assertEquals("invalid-move", Commands.invalidMove());
    }

    @Test
    public void moveMadeTest() {
        assertEquals("move-made 2 1 RGYP 4", Commands.moveMade(2, 1, "RGYP", 4));
        assertEquals("move-made 4 4 R 0", Commands.moveMade(4, 4, "R", 0));
    }

    @Test
    public void nextPlayerTest() {
        assertEquals("next-player Pug", Commands.nextPlayer("Pug"));
    }

    @Test
    public void gameOverWinnerTest() {
        assertEquals("game-over winner Nakor", Commands.gameOverWinner("Nakor"));
    }

    @Test
    public void gameOverDrawTest() {
        assertEquals("game-over draw", Commands.gameOverDraw());
    }
}