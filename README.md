# Ringgz Protocol for Lab Group 1 (2017/2018)

This repo contains a decription and simple Java implementation of the protocol used for the Ringgz game, the programming project for TCS Module 2 2017/2018 at the University of Twente.

## Getting started
As advised in the module guide (page 111), a Java class containing all commands is available. Just copy the [Commands.java](https://gitlab.com/arthurrump/ringgz.protocol/raw/master/src/main/java/ringgz/protocol/Commands.java) file to your project, and use it as follows:

```java
import ringgz.protocol.Commands;
String loginCommand = Commands.login("Pug", new Extension[] {});
loginCommand.split(" ")[0].equals(Commands.LOGIN); // True
```

You can use the methods in this class to create your messages, and the constants to parse the messages.

Documentation of all commands and constants is available as JavaDoc [here](https://arthurrump.gitlab.io/ringgz.protocol/ringgz/protocol/Commands.html#method.detail). Example protocol exchanges and textual description of the protocol flow can be found in the [docs folder](https://gitlab.com/arthurrump/ringgz.protocol/tree/master/docs).

## Include using a package manager
*Note: if you are confused by the following section, it's not meant for you. It's meant for people with custom build processes, but simply copying over the file works just as good.*

If you prefer, you can use Maven to include the commands, instead of copying the file to your project. To do so, include the following snippets in your `pom.xml` file:

```xml
<repositories>
    ...
    <repository>
        <id>ringgz.protocol</id>
        <url>https://www.myget.org/F/ringgz-protocol-lab1-2018/maven/</url>
    </repository>
    ...
</repositories>

<dependencies>
    ...
    <dependency>
        <groupId>ringgz.protocol</groupId>
        <artifactId>ringgz-protocol-lab1-2018</artifactId>
        <version>1.0.0</version>
    </dependency>
    ...
</dependencies>
```

Snippets for Gradle and Ivy can also be found at the [Maven feed](https://www.myget.org/feed/ringgz-protocol-lab1-2018/package/maven/ringgz.protocol/ringgz-protocol-lab1-2018).