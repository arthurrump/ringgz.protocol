import org.junit.jupiter.api.Test;
import ringgz.protocol.Commands;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for all commands for the leaderboard extension.
 * @author Arthur Rump
 */
public class LeaderboardCommandsTest {
    @Test
    public void getLeaderboardTest() {
        assertEquals("get-leaderboard", Commands.getLeaderboard());
    }

    @Test
    public void leaderboardTest() {
        // Use LinkedHashMap as it keeps the order. This is only required for the test,
        // the order shouldn't matter in an actual application.
        Map<String, Integer> leaderboard = new LinkedHashMap<>();
        leaderboard.put("Nakor", 53);
        leaderboard.put("MacrosTheBlack", 32);
        leaderboard.put("Pug", 31);
        leaderboard.put("Aglaranna", 23);
        leaderboard.put("Tomas", 22);
        leaderboard.put("Roo", 1);

        String expected = "leaderboard Nakor(53) MacrosTheBlack(32) Pug(31) Aglaranna(23) " +
                            "Tomas(22) Roo(1)";

        assertEquals(expected, Commands.leaderboard(leaderboard));
    }
}