# The basic protocol

This document describes the flow of the protocol if none of the extensions are enabled. To get a description of each message and its parameters, check out the [Javadoc](https://arthurrump.gitlab.io/ringgz.protocol/ringgz/protocol/Commands.html#method.detail). The *Method Detail* section is roughly in logical order, with messages specific to extensions at the end. Each method and constant is tagged with `[CLIENT]` or `[SERVER]` to indicate the sender of that message. Messages specific to extensions are tagged with `[EXT (extension name)]`.

## Flow of messages
Before sending messages the client connects to the server over a socket, which opens a channel through which the messages of the protocol can be send.

Format of the messages: `command <argument> [<optional argument>]`

1. Client: `login <username> [<extension1> <extension2>]`
2. Server: `login ok [<extension1> <extension2>]` if the login was succesful, otherwise `login fail <errorcode>`

    If the login was successful, then we can proceed, otherwise we go back to 1.

3. Client: `make-game <number of players>`

    Now the server will wait until there are enough players to fill a game, and then proceed. From here on all messages from the server are sent to all clients participating in the game, unless noted otherwise.

4. Server: `game-started <username>(<color1>[,<color2>]) <username>(<color1>[,<color2>]) [<username>(<color1>[,<color2>]) <username>(<color>)]`
5. Server: `next-player <username>`

    The username is of the next player who's next to make a move, and is actually able to make a valid move. If a player isn't able to make a valid move, they won't be asked to do so (and the username is of the player after them). If no player can make a valid move, go to 9.

6. Client: `make-move <row> <column> <color> <size>`

    Of course only the client whose username was sent in the previous message sends this message.

7. Server: `invalid-move` if the move the client sent was not valid, then go back to 6. This message only gets sent to the player making the move.
8. Server: `move-made <row> <column> <color> <size>`

    Then go back to 5.

9. Server: `game-over winner <username>` if there is one winner of the game, or `game-over draw` if the game is a draw.

Now the client can ask the server again to make a new game, going back to 3; or they can close the connection.

## Encoding of rings
Rings have a color and a size, indicated with a letter and a number.

| Color  | Code   |
|--------|--------|
| Red    | `R`    |
| Green  | `G`    |
| Yellow | `Y`    |
| Purple | `P`    |
| All    | `RGYP` |

| Size   | Code   |
|--------|--------|
| Small  | 0      |
| .      | 1      |
| .      | 2      |
| Large  | 3      |
| Base   | 4      |

## Login error codes
| Code | Error meaning                                                                                                  |
|------|----------------------------------------------------------------------------------------------------------------|
| 0    | The username is not in a valid format. A username can only contain letters (upper- and lowercase) and numbers. |
| 1    | The username is already in use by another client.                                                              |

## Examples
Here are a few examples of possible communication flow:

(C: client, S: server)

```
C: login Macros.the.Black
S: login fail 0
C: login MacrosTheBlack
S: login ok
C: make-game 2
S: game-started MacrosTheBlack(R,P) Nakor(Y,G)
S: next-player MacrosTheBlack
C: make-move 2 2 RGYP 4
S: move-made 2 2 RGYP 4
S: next-player Nakor
S: move-made 2 1 Y 0
S: next-player MacrosTheBlack
C: make-move 2 1 R 3
S: invalid-move
C: make-move 1 2 P 2
S: move-made 1 2 P 2
.
.
S: next-player Nakor
S: move-made 0 0 Y 0
S: next-player Nakor
S: move-made 1 0 Y 1
S: game-over winner Nakor
```

```
C: login Pug chat leaderboard
S: login ok chat
C: make-game 3
S: game-started Pug(P,R) Tomas(Y,R) Calis(G,R)
S: next-player Tomas
S: move-made 1 1 RGYP 4
S: next-player Calis
S: move-made 0 1 G 2
S: next-player Pug
C: make-move 1 2 P 0
S: move-made 1 2 P 0
S: next-player Tomas
.
.
game-over draw
```

## Handling of player disconnection
When a player disconnects (i.e. the socket connection is closed, or the server decides on a timeout) during a game, the server sends a chat message (if supported by the server) to all clients that indicated to support chat at login, and then the server lets an internal AI decide on the moves for that player, to enable the other players to keep playing.