import org.junit.jupiter.api.Test;
import ringgz.protocol.Commands;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for all commands for the challenge extension.
 * @author Arthur Rump
 */
public class ChallengeCommandsTest {
    @Test
    public void requestPlayersTest() {
        assertEquals("request-players", Commands.requestPlayers());
    }

    @Test
    public void playersTest() {
        assertEquals("players Pug Tomas MacrosTheBlack Aglaranna",
                Commands.players(new String[] {"Pug", "Tomas", "MacrosTheBlack", "Aglaranna"}));
    }

    @Test
    public void challengeTest() {
        assertEquals("challenge Pug MacrosTheBlack",
                Commands.challenge(new String[] {"Pug", "MacrosTheBlack"}));
    }

    @Test
    public void inviteTest() {
        assertEquals("invite Nakor Pug MacrosTheBlack",
                Commands.invite(new String[] {"Nakor", "Pug", "MacrosTheBlack"}));
    }

    @Test
    public void inviteAcceptedTest() {
        assertEquals("invite accepted", Commands.inviteAccepted());
    }

    @Test
    public void inviteDeniedTest() {
        assertEquals("invite denied", Commands.inviteDenied());
    }

    @Test
    public void challengeDeniedTest() {
        assertEquals("challenge denied", Commands.challengeDenied());
    }
}